#include <bits/stdc++.h>

using namespace std;

const int N = 101;
const int M = 1000000007;

int N1, N2, K1, K2;
long long memo[N][N][15][15];
long long f[N][N][2];

long long Calc(int n1, int n2, int k) {
	if (n1 < 0 || n2 < 0) return 0;
	if (n1 == 0 && n2 == 0) return 1;

	long long &res = f[n1][n2][k];
	if (res != -1) return res;

	res = 0;
	if (k == 0) {
		for (int i = 1; i <= min(n1, K1); i++)
			res = (res % M + Calc(n1 - i, n2, 1)) % M;
	} else {
		for (int i = 1; i <= min(n2, K2); i++)
			res = (res % M + Calc(n1, n2 - i, 0)) % M;
	}

	return res;
}

long long dp(int n1, int n2, int k1, int k2) {
	if (n1 < 0 || n2 < 0) return 0;
	if (n2 == 0 && n1 == 0) return 1;

	long long &res = memo[n1][n2][k1][k2];
	if (res != -1) return res;

	res = 0;
	if (k1 > 0) res = (res % M + dp(n1 - 1, n2, k1 - 1, K2) % M) % M;
	if (k2 > 0) res = (res % M + dp(n1, n2 - 1, K1, k2 - 1) % M) % M;

	return res;
}

int main() {
	#ifndef ONLINE_JUDGE
		freopen("in.txt", "r", stdin);
	#endif // ONLINE_JUDGE

	int T;	scanf("%d\n", &T);
	while (T--) {
		memset(memo, -1, sizeof memo);
		memset(f, -1, sizeof f);

		scanf("%d %d %d %d\n", &N1, &N2, &K1, &K2);
		printf("%lld\n", Calc(N1, N2, 0) + Calc(N1, N2, 1));
	}

	#ifndef ONLINE_JUDGE
		fprintf(stderr, "\nTime elapsed: %.5lf ms\n", (double) 1000 * clock() / CLOCKS_PER_SEC);
	#endif // ONLINE_JUDGE

	return 0;
}
