#include <bits/stdc++.h>

using namespace std;

const int N = 25;
const int M = 1000000007;

vector <int> all, firstDiff;

int C[N + 1][N + 1], GCD[N + 1][N + 1];
int A[N + 1];
int n, k, L, mask;

void solve() {
  long long ans = 0;
  scanf("%d %d %d\n", &n, &k, &L);
  for (int i = 1; i <= N; i++) {
    scanf("%d ", &A[i]);
    mask |= (1 << A[i]);
  }

  for (int i = 0; i < firstDiff[L + 1]; i++) {
    if (mask & ~all[i]) continue;

    int x = all[i] & ~mask;
    int X = __builtin_popcount(x);

    if (X > k) continue;
    if (X == k) {
      ans = (ans + 1) % M;
      continue;
    }

    int Y = k - X;
    ans = (ans % M + C[__builtin_popcount(all[i]) + Y - 1][Y - 1] % M) % M;
  }

  printf("%d\n", ans);
}

int main() {
  #ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
  #endif // ONLINE_JUDGE

  for (int i = 1; i <= N; i++)
    for (int j = 1; j <= N; j++)
      GCD[i][j] = __gcd(i, j);

  for (int i = 1; i <= N; i++) C[i][0] = i;
  for (int i = 1; i <= N; i++)
    for (int j = 1; j <= N; j++)
      if (i < j) continue;
      else C[i][j] = (C[i-1][j] % M + C[i-1][j-1] % M) % M;

  all.push_back(2);
  firstDiff.push_back(-1);
  firstDiff.push_back(0);

  for (int k = 2; k <= N; k++) {
    firstDiff.push_back(all.size());

    for (int i = 0; i < (int) all.size(); i++) {
      if ((all[i] >> k) & 1) continue;

      int j = 0;
      for (j = 1; j < k; j++)
        if ((all[i] >> j) & 1
            && (GCD[j][k] != k && ((all[i] >> GCD[j][k]) & 1))) break;

      if (j == k) all.push_back((1 << k) | all[i]);
    }
  }
  firstDiff.push_back(all.size());

  int T;  scanf("%d", &T);
  while (T--) solve();

  #ifndef ONLINE_JUDGE
    fprintf(stderr, "\nTime elapsed: %d ms", 1000 * clock() / CLOCKS_PER_SEC);
  #endif // ONLINE_JUDGE

  return 0;
}
